Test Sthima
===========

# Requirements
Python 3.6
Pipenv

# Clone/unzip the content
cd /var/www
git clone [repo] teste_sthima
cd teste_sthima

# Create the virtualenv and install dependences
pipenv --python 3.6
pipenv install

# Apply migrations and run the project
pipenv shell
python manage.py migrate
python manage.py runserver

# Access the url
browser http://127.0.0.1:8000/
