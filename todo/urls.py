from django.conf.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter

from todo import views

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'tasks', views.TaskViewSet)

urlpatterns = [
    path('', views.index),
    path('api/', include(router.urls))
]
