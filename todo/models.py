from django.db import models


class Task(models.Model):
    name = models.CharField(max_length=255)
    order = models.IntegerField(default=0)
    done = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Item'
        verbose_name_plural = 'Item'
