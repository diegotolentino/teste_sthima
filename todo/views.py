from django.shortcuts import render_to_response
from rest_framework import permissions
from rest_framework import viewsets

from todo.models import Task
from todo.serializers import TaskSerializer


def index(request):
    """
    Inicio do carregamento do frontend

    :param request:
    :return:
    """
    return render_to_response('todo/index.html')


class TaskViewSet(viewsets.ModelViewSet):
    """
    Api do todo list
    """
    queryset = Task.objects.all().order_by('order')
    serializer_class = TaskSerializer
    permission_classes = (permissions.AllowAny,)
