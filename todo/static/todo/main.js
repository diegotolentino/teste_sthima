"use strict";

class Connection {
    constructor(parent) {
        this.parent = parent
    }

    /**
     * Retrive the task list
     *
     * @param onSucess
     */
    fetch(onSucess) {
        let self = this;
        $.ajax({
            url: "/api/tasks/",
            method: 'GET',
        }).done(function (data) {
            onSucess(data);
        });
    };

    /**
     * Update the task
     *
     * @param task
     * @param onSucess
     */
    save(task, onSucess) {
        let options = {};
        if ('pk' in task) {
            /*update*/
            options = {
                url: "/api/tasks/" + task.pk + "/",
                method: 'PUT'
            }
        } else {
            /*create*/
            options = {
                url: "/api/tasks/",
                method: 'POST',
            }
        }
        options['data'] = task;
        $.ajax(options).done(function (data) {
            console.log(data);
            onSucess(data);
        });
    }

    /**
     * Delete the task
     *
     * @param task
     * @param onSucess
     */
    delete(pk, onSucess) {
        $.ajax({
            url: "/api/tasks/" + pk + "/",
            method: 'DELETE',
        }).done(function () {
            onSucess();
        });
    }
}


class TaskList {
    constructor() {
        let self = this;

        /* In memory task list*/
        this.data = [];

        /* Api interface */
        this.conn = new Connection(this);

        /* add enter newTask keyevent */
        this.newTask = $('#newTask');
        this.newTask.keypress(function (e) {
            if (e.which == 13) { // Enter key
                self.addTask();
            }
        });

        /* Add delete event */
        $('.list-item').on('click', 'button.delete', function () {
            let li = $(this).closest('li');
            self.removeTask($(li).data('pk'));
        });

        /* Add check done event */
        $('.list-item').on('change', 'input[type="checkbox"]', function () {

            let li = $(this).closest('li');
            let task = self.filterTask($(li).data('pk'));

            task['done'] = this.checked;

            /* save and reload list*/
            self.conn.save(task, function () {
                self.refreshData();
            });
        });

        /* Add edit event */
        $('.list-item').on('dblclick', 'li', function () {
            self.startEditing($(this).data('pk'));
        });

        /* Save edit events */
        $('.list-item').on('keypress', '.text-input', function (e) {
            if (e.which == 13) { // Enter key
                self.endEditing(this);
            }
        });

        $('.list-item').on('blur', '.text-input', function (e) {
            self.endEditing(this);
        });

        /* add the sort event */
        this.elListItems = $('.list-item');
        this.elListItems.sortable({
            stop: function () {
                self.updateSort()
            }
        });

        /* trigger the refreshData */
        this.refreshData();
    }

    /**
     * Reread the api
     */
    refreshData() {
        let self = this;
        this.conn.fetch(function (data) {
            self.data = data;
            self.refresh();
        });
    }

    /**
     * Update the order positions after a sort event
     */
    updateSort() {
        let self = this;
        $('.list-item li').each(function (index, el) {
            let task = self.filterTask($(el).data('pk'));
            task['order'] = index;
            self.conn.save(task, function () {
            })
        });
        console.log(this.data)
    }

    /**
     * Refresh the render list items
     */
    refresh() {
        let content = '';
        for (let item of this.data) {
            content += this.render(item, item.pk == this.editindPk);
        }
        this.elListItems.html(content);
    }

    /**
     * Render a single task item
     */
    render(item, editindPk = false) {
        let contentItem = '';
        contentItem += '<li class="todo-item ' + (item.done ? 'done' : '') + '" data-pk="' + item.pk + '">\n';
        contentItem += '    <input type="checkbox" class="checkbox"' + (item.done ? 'checked' : '') + '>\n';

        if (editindPk) {
            contentItem += '    <input type="text" class="text-input" value="' + item.name + '">\n';
        } else {
            contentItem += '    <label for="checkbox">' + item.name + '</label>\n';
        }
        contentItem += '  <button class="delete">X</button>\n';
        contentItem += '</li>';

        return contentItem
    }

    /**
     * handler the add event, send info to server and put on list
     */
    addTask() {
        let name = this.newTask.val().trim();
        let self = this;
        if (name) {
            /* find the greatest order*/
            let order = 0;
            for (let item of this.data) {
                if (order <= item.order) {
                    order = item.order + 1
                }
            }

            /* save and add task to list*/
            this.conn.save({name: name, order: order}, function (data) {
                self.data.push(data);
                self.refresh();
            });

            this.newTask.val('');
        }
    }

    /**
     * Do the task delete
     * @param pk
     */
    removeTask(pk) {
        let self = this;

        this.conn.delete(pk, function () {
            self.data = $.grep(self.data, function (task) {
                return task.pk != pk;
            });
            self.refresh();
        });
    }

    /**
     * Start editing of task
     * @param pk
     */
    startEditing(pk) {
        this.editindPk = pk;
        this.refresh();
        $('li .text-input').focus();
    }

    /**
     * Stop and save/delete the editing task
     *
     * @param el
     * @returns {boolean}
     */
    endEditing(el) {
        console.log('dentro do endEditing');
        if (!this.editindPk) {
            return false;
        }

        let name = $(el).val().trim();

        if (name === "") {
            /* Delete */
            this.removeTask(this.editindPk);
            this.editindPk = null;
        } else {
            /* Update */
            let self = this;
            let task = this.filterTask(this.editindPk)
            task['name'] = name;

            this.conn.save(task, function (data) {
                self.editindPk = null;
                self.refreshData();
            });
        }
    }

    /**
     * Filter the task by pk
     * @param pk
     * @returns {*}
     */
    filterTask(pk) {
        for (let item of this.data) {
            if (item.pk == pk) {
                return item;
            }
        }
        return null;
    }
}